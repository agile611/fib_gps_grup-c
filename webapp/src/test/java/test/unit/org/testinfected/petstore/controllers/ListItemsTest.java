package test.unit.org.testinfected.petstore.controllers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.junit.Assert.*;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testinfected.petstore.controllers.ListItems;
import org.testinfected.petstore.product.Item;

import com.vtence.molecule.Request;
import com.vtence.molecule.Response;

import static test.support.org.testinfected.petstore.builders.ItemBuilder.a;
import static test.support.org.testinfected.petstore.builders.ProductBuilder.aProduct;

import static test.support.org.testinfected.petstore.builders.FakeProductNumber.aProductNumber;

/**
 * @author Carlos Lázaro Costa (carlos.lazaro.costa@est.fib.upc.edu)
 */
public class ListItemsTest {
	
	private final String productNumber = aProductNumber();
	
	private FakeItemInventory itemInventory;
	private FakeViewAvailableItems fakeView;
	private ListItems listItems;
	
	@Before
	public void setUp() {
		itemInventory = new FakeItemInventory();
		itemInventory.addItems(a(aProduct().withNumber(productNumber)).build());
		fakeView = new FakeViewAvailableItems();
		listItems = new ListItems(itemInventory, fakeView);
	}
	
	@After
	public void teardown() {
		itemInventory = null;
		fakeView = null;
		listItems = null;
	}
	
	@Test
	public void testListItemsByIdentifier() {
		try {
			listItems.handle(new Request().addParameter("product", productNumber), new Response());
			
			assertThat("available items", fakeView.getAvailableItems(), everyItem(hasProductNumber(productNumber)));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	/* Support methods */
	
    private Matcher<Item> hasProductNumber(final String number) {
        return new FeatureMatcher<Item, String>(equalTo(number), "has product number", "product number") {
            protected String featureValueOf(Item actual) {
                return actual.getProductNumber();
            }
        };
    }

}
