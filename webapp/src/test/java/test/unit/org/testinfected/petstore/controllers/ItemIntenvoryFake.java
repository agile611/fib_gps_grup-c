package test.unit.org.testinfected.petstore.controllers;

import java.util.List;

import org.testinfected.petstore.product.DuplicateItemException;
import org.testinfected.petstore.product.Item;
import org.testinfected.petstore.product.ItemInventory;
import org.testinfected.petstore.product.ItemNumber;

public class ItemIntenvoryFake implements ItemInventory {

	@Override
	public List<Item> findByProductNumber(String productNumber) {
		return null;
	}

	@Override
	public Item find(ItemNumber itemNumber) {
		return new Item(itemNumber, null, null);
	}

	@Override
	public void add(Item item) throws DuplicateItemException {
		
	}

}
